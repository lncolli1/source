import java.util.Scanner;

public class CombatRound {
    public static void main(String[] args){
       int voldemortsHealth = 701;
       int swordDamage = 100;
       System.out.println("A wild Voldemort has appeared"); 
       
       System.out.println("Enter");
       System.out.println("1 to attack with the Sword of Gryffindor");
       System.out.println("2 to cast a spell");
       System.out.println("3 to wrap yourself in the invisibility cloak and run");
       
       int choice;
       Scanner inputReader = new Scanner(System.in);
       choice = inputReader.nextInt();
       
       
       if(choice == 1){
           voldemortsHealth = voldemortsHealth - swordDamage;
           System.out.println("Horcrux Destroyed!");
           System.out.println(voldemortsHealth + " health remaining");
       } else if (choice == 2){
           System.out.println("Casting a spell at Voldemort.");
           voldemortsHealth = voldemortsHealth / 2;
           System.out.println("Damn Neville, that's strong!");
           System.out.println(voldemortsHealth + " health remaining");
       }else if (choice == 3){
           System.out.println("You wrap yourself in the invisibility cloak and run away.");
       }else {
           System.out.println("Dummy, input valid value");
       }
    }
}