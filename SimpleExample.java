import java.util.Scanner;

public class SimpleExample{
    public static void main(String[] args){
        System.out.print("Enter your name: ");
        Scanner inputReader = new Scanner(System.in);
        String userName = inputReader.next();
        
        if( userName.equals("Aaron") ) {
            System.out.println("Your are a dummy!");
        } else {
            System.out.println("Hello intelligent human being.");
        }
    }
}