/*
 * Author: 1583 MW Section
 * Program: Prime Number Generator
 * Date: 27 Jan 2016
*/
public class GeneratePrimes2{
    public static void main(String[] args){
        int upperLimit = 1000000; // generate all the primes up to this limit
        int numPrimesFound = 0; // keep track of the number of primes found
        long beginning = (new Date()).getTime();
        // for every number between 2 and the limit,
        // check if it is prime
        for(int potentialPrime = 2; potentialPrime <= upperLimit; potentialPrime++){
            // we will assume the number is prime and prove this false if
            // we find any divisors
            boolean isPrime = isNumberPrime(int num); 
            
            // check all potential divisors. These are the numbers from 2
            // to sqrt(potentialPrime)
            for(int potentialDivisor = 2; potentialDivisor <= Math.sqrt(potentialPrime); potentialDivisor++){
                // if we find that any number evenly divides it, then we know 
                // the number is not prime
                if(potentialPrime % potentialDivisor == 0){
                    isPrime = false;
                }
            }
            
            // if the number is prime, then...
            if(isPrime){
                numPrimesFound++; // increment the number of primes found
                // and print out the number. 
                System.out.print(potentialPrime + "\t");
                // every 10 numbers, print a new line.
                if(numPrimesFound % 10 == 0){
                    System.out.println();
                }
            }
        }
        // closing message states how many primes were found.
        System.out.println("\nNumber of primes below " + upperLimit + ": " + numPrimesFound);
    }
    
    public static boolean isNumberPrime(int num){
        
    }
}